package adam.javafun.reactivefun.beans;

import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import adam.javafun.reactivefun.domain.model.Student;
import adam.javafun.reactivefun.domain.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DataGenerator {

    @Value("${initializeData.shouldinitialize}")
    private boolean generateData;

    @Value("${initializeData.numberOfRecords:100}")
    private int numberOfRecords;

    private final StudentRepository repository;

    private final DataRandomizer randomizer;

    public DataGenerator(StudentRepository repository, DataRandomizer randomizer) {
        this.repository = repository;
        this.randomizer = randomizer;
    }

    @PostConstruct
    public void generateData() {
        if (generateData) {
            addStudentsToDatabase();
        }
    }

    private void addStudentsToDatabase() {
        repository.count().subscribe(x -> {
            if (x == 0) {
                IntStream.range(0, numberOfRecords).forEach(i -> repository.save(randomizer.randomizeData(Student.class)).subscribe(student -> log.info("Saving student {} to database", student.getName())));
            }
        });

    }
}
