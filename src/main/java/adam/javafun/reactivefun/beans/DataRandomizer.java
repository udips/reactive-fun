package adam.javafun.reactivefun.beans;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DataRandomizer {

    <T> T randomizeData(Class<T> targetClass) {
        Constructor constructor = targetClass.getConstructors()[0];
        Class[] parameterTypes = constructor.getParameterTypes();

        List<Object> values = new ArrayList<>();
        values.add(null);
        for (int i = 1; i < parameterTypes.length; i++) {
            Class c = parameterTypes[i];
            if (c.isAssignableFrom(String.class)) {
                values.add(randomizeString());
            }
            if (c.isAssignableFrom(Long.class)) {
                values.add(null);
            }
        }
        try {
            return (T) constructor.newInstance(values.toArray());
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            log.error(e.getLocalizedMessage());
        }
        return null;
    }

    private String randomizeString() {
        return RandomStringUtils.random(10, true, false);
    }
}
