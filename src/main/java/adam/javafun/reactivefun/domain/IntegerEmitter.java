package adam.javafun.reactivefun.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class IntegerEmitter {

    private Random rand = new Random();

    private List<Consumer<Integer>> consumerList = new ArrayList<>();

    private boolean shouldEmit = true;

    void registerConsumer(Consumer<Integer> cons) {
        log.info("Registering consumer {}", cons);
        consumerList.add(cons);
    }

    @PostConstruct
    public void emit() {
        shouldEmit = true;
        ForkJoinPool.commonPool().submit(() -> {
            while (shouldEmit) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    log.error("whoopsie daisy");
                    Thread.currentThread().interrupt();
                }
                int i = rand.nextInt();
                consumerList.forEach(consumer -> consumer.accept(i));
            }
            log.info("############## EMITTER STOPPING");
        });
    }

    void stopEmission() {
        shouldEmit = false;
        consumerList.clear();
    }

}
