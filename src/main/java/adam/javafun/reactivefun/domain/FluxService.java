package adam.javafun.reactivefun.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import adam.javafun.reactivefun.domain.model.Student;
import adam.javafun.reactivefun.domain.repository.StudentRepository;
import reactor.core.publisher.Flux;

@Service
public class FluxService {

    public Flux<Integer> fluxFun() {
        return Flux.range(1, 10);
    }

    @Autowired
    IntegerEmitter emitter;

    @Autowired
    StudentRepository studentRepository;

    public Flux<Integer> fluxFun2() {
        return Flux.create(sink -> emitter.registerConsumer(sink::next));
    }

    public void toggleEmitter(boolean toggle) {
        if (toggle) {
            emitter.emit();
        } else {
            emitter.stopEmission();
        }
    }

    public Flux<Student> getStudents() {
        return studentRepository.findAll();
    }
}
