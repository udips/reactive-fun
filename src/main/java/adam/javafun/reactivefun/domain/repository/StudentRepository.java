package adam.javafun.reactivefun.domain.repository;

import org.springframework.data.annotation.Id;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import adam.javafun.reactivefun.domain.model.Student;

public interface StudentRepository extends ReactiveCrudRepository<Student, Id> {

}
