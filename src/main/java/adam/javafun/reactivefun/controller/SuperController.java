package adam.javafun.reactivefun.controller;

import adam.javafun.reactivefun.beans.DataGenerator;
import adam.javafun.reactivefun.domain.FluxService;
import adam.javafun.reactivefun.domain.model.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;

import java.net.URI;
import java.time.Duration;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@Slf4j
public class SuperController {

    @Bean
    RouterFunction<ServerResponse> apiRedirect() {
        return route(GET("/"), req ->
                ServerResponse.temporaryRedirect(URI.create("/api"))
                        .build());
    }

    @RestController
    @RequestMapping("/api")
    public class HelloController {

        @Value("${application.name}")
        String appName;

        private Integer abc = 0;

        @Autowired
        FluxService fs;

        @Autowired
        DataGenerator generator;

        @GetMapping
        public String sayHello() {
            fs.fluxFun().subscribe(msg -> log.info(msg.toString()));
            return "hello from " + appName;
        }

        //call this mapping to obtain generated list of logs comming from IntegerEmitter
        @GetMapping("/asd")
        public void fun2() {
            abc++;
            fs.fluxFun2().subscribe(msg -> log.info(msg.toString() + " %%%%%%%%% consumer nr " + abc));
        }

        @GetMapping("/asd/startemission")
        public void fun2start() {
            log.info("Starting emission");
            fs.toggleEmitter(true);
        }

        @GetMapping("/asd/stopemission")
        public void fun2stop() {
            log.info("Stopping emission");
            fs.toggleEmitter(false);
        }

        @GetMapping("/students")
        public Flux<Student> getStudents() {
            return fs.getStudents().delayElements(Duration.ofSeconds(1)).doOnNext(item -> log.info(item.getName()))
                    .doOnComplete(() -> log.info("##########%%%%%%%%#######"));
        }
    }
}
