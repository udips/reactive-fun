package adam.javafun.reactivefun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class ReactiveFunApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveFunApplication.class, args);
    }

}
