import {Component, NgZone} from '@angular/core';
import {SearchService} from "./searchservice.service";
import {Student} from "./student.data";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  constructor(private searchService: SearchService, private zone: NgZone) {

  }

  title = 'frontend';

  results: Student[] = [];

  obs: Observable<Student[]> = new Observable((observer) => {
    let eventSource = this.searchService.getData();
    eventSource.onmessage = (event) => {
      this.zone.run(() => {
        console.log('Received event: ', event);
        let parse = JSON.parse(event.data);
        observer.next(parse);
        this.results.push(new Student(parse.id, parse.name, parse.passportNumber));
      });
    };
    eventSource.onopen = ev => console.log("OPENED", ev);
    eventSource.onerror = error => {
      console.log("error", error);
      console.log("readystate", eventSource.readyState);
      if (eventSource.readyState === 0) {
        eventSource.close();
        observer.complete();
      }
    }
  });

  getStudents() {
    this.obs.subscribe();
  }
}

