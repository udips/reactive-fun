import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor() {
  }

  getData():EventSource {
    return new EventSource("/api/students");
  }
}
