export class Student {
  private id: string;
  name: string;
  private passportNumber: string;

  public constructor(id: string, name: string, passportNumber: string) {
    this.id = id;
    this.name = name;
    this.passportNumber = passportNumber;
  }
}
